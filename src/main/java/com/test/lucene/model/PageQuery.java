package com.test.lucene.model;/**
 * Created by Administrator on 2019/11/12/012.
 */

import com.github.pagehelper.PageInfo;
import lombok.Data;
import java.util.List;
import java.util.Map;

/**
 * @ClassName PageQuery
 * @Description TODO
 * @Author yb
 * @Date 2019/11/12/01217:51
 * @Version 1.0
 **/
@Data
public class PageQuery {
    private PageInfo pageInfo;
    /**
     * 排序字段
     */
    private Sort sort;
    /**
     * 查询参数类
     */
    private Product params;
    /**
     * 返回结果集
     */
    private List<Product> results;
    /**
     * 不在T类中的参数
     */
    private Map<String, String> queryParam;
}
