package com.test.lucene.model;/**
 * Created by Administrator on 2019/11/14/014.
 */

import lombok.Data;

import java.util.Date;

/**
 * @ClassName Product
 * @Description TODO
 * @Author yb
 * @Date 2019/11/14/0149:57
 * @Version 1.0
 **/
@Data
public class Product {
    private Integer id;

    private Long cid1;

    private String cid1Name;

    private Double commission;

    private Double commissionShare;

    private Integer bindType;

    private String brandCode;

    private String brandName;

    private Long comments;

    private Double discount;

    private Integer eliteId;

    private String eliteName;

    private Date getEndTime;

    private Date getStartTime;

    private String link;

    private String materialUrl;

    private String owner;

    private Double pingouPrice;

    private Long pingouTmCount;

    private String pingouUrl;

    private Double price;

    private Double quota;

    private Long shopId;

    private String shopName;

    private Long skuId;

    private String skuName;

    private Long spuid;

    private String url;

    private Date useEndTime;

    private Date useStartTime;

    private String clickurl;

    private Double discountRate;

    private Date updateTime;

    private Date createTime;
    private Integer page;
    private Integer limit;
    private String desc;
}
