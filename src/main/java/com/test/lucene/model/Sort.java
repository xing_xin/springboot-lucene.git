package com.test.lucene.model;/**
 * Created by Administrator on 2019/11/12/012.
 */

import lombok.Data;

/**
 * @ClassName Sort
 * @Description TODO
 * @Author yb
 * @Date 2019/11/12/01219:29
 * @Version 1.0
 **/
@Data
public class Sort {
    private String order;
    private String Field;
}
