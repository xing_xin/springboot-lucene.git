package com.test.lucene.service;/**
 * Created by Administrator on 2019/11/12/012.
 */

import com.test.lucene.model.Product;
import com.test.lucene.model.PageQuery;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @ClassName ILuceneService
 * @Description TODO
 * @Author yb
 * @Date 2019/11/12/01218:00
 * @Version 1.0
 **/
public interface LuceneService {
    /**
     * 增加索引
     * @param list
     * @throws IOException
     */
    public void createProductIndex(List<Product> list) throws IOException;

    /**
     * 查询
     * @param pageQuery
     * @return
     * @throws Exception
     * @throws ParseException
     */
    public PageQuery searchProduct(PageQuery pageQuery) throws Exception, ParseException;

    /**
     *删除
     * @param id
     * @throws IOException
     */
    public void deleteProductIndexById(String id) throws IOException;
}
