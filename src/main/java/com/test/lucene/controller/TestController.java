package com.test.lucene.controller;/**
 * Created by Administrator on 2019/11/14/014.
 */

import com.github.pagehelper.PageInfo;
import com.test.lucene.model.PageQuery;
import com.test.lucene.model.Product;
import com.test.lucene.service.LuceneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author yb
 * @Date 2019/11/14/0149:50
 * @Version 1.0
 **/
@Controller
public class TestController {
    @Autowired
    private LuceneService luceneService;

    @RequestMapping("creat")
    @ResponseBody
    public String create() throws Exception{
        List<Product> list = new ArrayList<Product>();
        Product product = new Product();
        product.setId(1);
        product.setBindType(10);
        product.setBrandCode("001");
        product.setBrandName("联想");
        product.setCid1(10000L);
        product.setCid1Name("电脑");
        product.setClickurl("www.baidu.com");
        product.setComments(100L);
        product.setCommission(10.0);
        product.setCommissionShare(20.0);
        product.setCreateTime(new Date());
        product.setDesc("desc");
        product.setDiscount(10.0);
        product.setDiscountRate(30.0);
        product.setSkuId(635373L);
        product.setPrice(999.01);
        product.setSkuName("硕扬 i7升十二线程/RX560 4G独显/16G内存办公游戏台式组装电脑主机DIY组装机");
        list.add(product);
        luceneService.createProductIndex(list);
        return "hello world" ;
    }

    @RequestMapping("find")
    @ResponseBody
    public PageInfo<Product> find(@RequestBody Product pro) throws Exception{
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageNum(pro.getPage());
        pageInfo.setPageSize(pro.getLimit());
        PageQuery pageQuery = new PageQuery();
        pageQuery.setPageInfo(pageInfo);
        pageQuery.setParams(pro);
        PageQuery result  = luceneService.searchProduct(pageQuery);

        PageInfo<Product> pageResult = new PageInfo<Product>();
        pageResult.setPageSize(pro.getLimit());
        pageResult.setPageNum(pro.getPage());
        pageResult.setTotal(result.getPageInfo().getTotal());
        pageResult.setList(result.getResults());
        return pageResult;
    }
}
